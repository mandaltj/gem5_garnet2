/*
 * Copyright (c) 2008 Princeton University
 * Copyright (c) 2016 Georgia Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Niket Agarwal
 *          Tushar Krishna
 */


#include "mem/ruby/network/garnet2.0/NetworkLink.hh"

#include "mem/ruby/network/garnet2.0/CreditLink.hh"

std::vector<NetworkLink::APU> NetworkLink::APU_policy = apu_policy_init();

NetworkLink::NetworkLink(const Params *p)
    : ClockedObject(p), Consumer(this), m_id(p->link_id),
      m_type(NUM_LINK_TYPES_),
      m_latency(p->link_latency),
      linkBuffer(new flitBuffer()), link_consumer(nullptr),
      link_srcQueue(nullptr), m_link_utilized(0),
      m_vc_load(p->vcs_per_vnet * p->virt_nets),
      m_virt_nets(p->virt_nets)
{
    int num_vnets = (p->supported_vnets).size();
    assert(num_vnets > 0);
    mVnets.resize(num_vnets);
    bitWidth = p->width;
    for (int i = 0; i < num_vnets; i++) {
        mVnets[i] = p->supported_vnets[i];
    }
    enSNI = p->sni;
}

NetworkLink::~NetworkLink()
{
    delete linkBuffer;
}

void
NetworkLink::setLinkConsumer(Consumer *consumer)
{
    link_consumer = consumer;
}

void
NetworkLink::setVcsPerVnet(uint32_t consumerVcs)
{
    m_vc_load.resize(m_virt_nets * consumerVcs);
}

void
NetworkLink::setSourceQueue(flitBuffer *srcQueue, ClockedObject *srcClockObj)
{
    link_srcQueue = srcQueue;
    src_object = srcClockObj;
}

void
NetworkLink::wakeup()
{
    DPRINTF(RubyNetwork, "Woke up to transfer from %s to %s\n",
    src_object->name(), link_consumer->getName());
    assert(curTick() == clockEdge());
    if (link_srcQueue->isReady(curTick())) {
        flit *t_flit = link_srcQueue->getTopFlit();
        DPRINTF(RubyNetwork, "Transmission will finish at %ld :%s\n",
                clockEdge(m_latency), *t_flit);
        DPRINTF(RubySNI, "Transmission will finish at %ld :%s\n",
                clockEdge(m_latency), *t_flit);

        DPRINTF(RubySNI, "Flit Type: %s, VNET Type: %s\n",
                t_flit->get_type(), t_flit->get_vnet());
        //Added by TJ

        DPRINTF(RubySNI, "SNI: Link ID:%d\n", m_id);
        DPRINTF(RubySNI, "SNI: Before SNI\n");
        if (enSNI && (t_flit->get_type()!=CREDIT_)) {
            //TODO:Check whether the flit passes security check or not
            DPRINTF(RubySNI, "SNI: Entered SNI\n");
            DPRINTF(RubySNI, "SNI: Message:%s\n",
                    *(t_flit->get_msg_ptr()));
            DPRINTF(RubySNI, "Request Type:%s\n",
                    (t_flit->get_msg_ptr())->getType());
            bool apu_result = apu_check(t_flit);
            DPRINTF(RubySNI, "SNI: APU Check Done\n");
            if (!apu_result){
                //Machine check failure
                DPRINTF(RubySNI, "SNI: Test Failed\n");
                assert(false);
            }else{
                DPRINTF(RubySNI, "SNI: Test Passed\n");
                //Do nothing, continue
            }
            DPRINTF(RubySNI, "SNI: Leaving SNI\n");
        }
        DPRINTF(RubySNI, "SNI: After SNI\n");

        if (m_type != NUM_LINK_TYPES_) {
            // Only for assertions and debug messages
            assert(t_flit->m_width == bitWidth);
            assert((std::find(mVnets.begin(), mVnets.end(),
                t_flit->get_vnet()) != mVnets.end()) ||
                (std::find(mVnets.begin(), mVnets.end(), -1) != mVnets.end()));
        }
        t_flit->set_time(clockEdge(m_latency));
        linkBuffer->insert(t_flit);
        link_consumer->scheduleEventAbsolute(clockEdge(m_latency));
        m_link_utilized++;
        m_vc_load[t_flit->get_vc()]++;
    }

    if (!link_srcQueue->isEmpty()) {
        scheduleEvent(Cycles(1));;
    }
}

void
NetworkLink::resetStats()
{
    for (int i = 0; i < m_vc_load.size(); i++) {
        m_vc_load[i] = 0;
    }

    m_link_utilized = 0;
}

NetworkLink *
NetworkLinkParams::create()
{
    return new NetworkLink(this);
}

CreditLink *
CreditLinkParams::create()
{
    return new CreditLink(this);
}

uint32_t
NetworkLink::functionalWrite(Packet *pkt)
{
    return linkBuffer->functionalWrite(pkt);
}

//Added by TJ
std::vector<NetworkLink::APU> NetworkLink::apu_policy_init(){
    std::vector<APU> APU_temp;

    std::string filename = "my_benchmarks/apu_init/apu_init.txt";
    std::ifstream infile(filename);
    if (!infile) {
        infile.close();
        //std::cout<<"Current Directory: "<<get_current_dir()<<'\n';
        assert(false);
    }

    uint64_t id;
    uint64_t start_addr;
    uint64_t end_addr;
    uint64_t perm;

    while (infile >> id >> start_addr >> end_addr >> perm){
        APU_temp.emplace_back(id, start_addr, end_addr, perm);
    }

    return APU_temp;
}

bool NetworkLink::apu_check(flit *t_flit){
    Addr addr_check = (t_flit->get_msg_ptr())->getaddr();
    DPRINTF(RubySNI, "SNI: APU_CHECK: addr_check obtained\n");

    //THe chiplet_id is being extracted from the
    //L2Cahce-0/1/2/3... NodeID number Check getRequestor()
    //in RequestMsg/ResponseMsg and then the MachineID structure
    //Since each chiplet has a single L2 Cache,
    //the node number of L2 cache will be equal to the Chiplet ID

    unsigned int requestor_id = -1;
    unsigned int responder_id = -1;
    unsigned int chiplet_id = -1;

    if (t_flit->get_vnet() == 2){
        DPRINTF(RubySNI, "SNI: APU_CHECK: requestor_id obtained\n");
        requestor_id = (unsigned int)((t_flit->get_msg_ptr())
                       ->getRequestor()).getNum();
        chiplet_id = requestor_id%8;
    }
    else if (t_flit->get_vnet() == 4){
        DPRINTF(RubySNI, "SNI: APU_CHECK: responder_id obtained\n");
        responder_id = (unsigned int)((t_flit->get_msg_ptr())
                      ->getSender()).getNum();
        chiplet_id = responder_id%8;
    }

    //unsigned int requestor_id = (unsigned int)((t_flit->get_msg_ptr())
    //                          ->getRequestor()).getNum();

    if (requestor_id == -1 && responder_id == -1){
        assert(false);
    }

    int apu_policy_size = APU_policy.size();
    DPRINTF(RubySNI, "TJ: Requestor:%d, Chiplet_ID:%d, Addr:%d,
            APU_size:%d\n", requestor_id, chiplet_id, addr_check,
            apu_policy_size);


    if (requestor_id != -1) {
    unsigned int request_type = (t_flit->get_msg_ptr())->getType();
    for (int i=0; i<apu_policy_size; i++){
        //Read type - GETX and GETS
        //Write type -
            if ((addr_check>=APU_policy[i].apuaddr_start)
               && (addr_check<=APU_policy[i].apuaddr_end)){
                  if (chiplet_id==0){
                      DPRINTF(RubySNI, "SNI: Segmentation Fault Debug L1\n");
                      if ((APU_policy[i].apumid & 0x1) == 0){
                           //DPRINTF(RubySNI, "SNI:
                           //Segmentation Fault Debug L2\n");
                           assert(false);
                           return false;
                      }
                      else{
                           //DPRINTF(RubySNI, "SNI:
                           //Segmentation Fault Debug L3\n");
                           return true;
                      }
                  }
                  else if (chiplet_id==1){
                      if ((APU_policy[i].apumid & 0x2) == 0){
                           assert(false);
                           return false;
                      }else{
                           return true;
                      }
                  }
                  else if (chiplet_id==2){
                      if ((APU_policy[i].apumid & 0x4) == 0){
                          assert(false);
                          return false;
                      }else{
                          return true;
                      }
                  }
                  else if (chiplet_id==3){
                      if ((APU_policy[i].apumid & 0x8) == 0){
                          assert(false);
                          return false;
                      }else{
                          return true;
                      }
                  }
                  else if (chiplet_id==4){
                      if ((APU_policy[i].apumid & 0x10) == 0){
                          assert(false);
                          return false;
                      }else{
                          return true;
                      }
                  }
                  else if (chiplet_id==5){
                      if ((APU_policy[i].apumid & 0x20) == 0){
                           assert(false);
                           return false;
                      }else{
                           return true;
                      }
                  }
                  else if (chiplet_id==6){
                      if ((APU_policy[i].apumid & 0x40) == 0){
                           assert(false);
                           return false;
                      }else{
                           return true;
                      }
                  }
                  else if (chiplet_id==7){
                      if ((APU_policy[i].apumid & 0x80) == 0){
                          assert(false);
                          return false;
                      }else{
                          return true;
                      }
                  }
                DPRINTF(RubySNI, "SNI: Addr Check passed\n");
                //Pass-Do Nothing!!
                //Add delay/latency for this check
            }
            else{
                //Fails
                //So we want to generate a response;
                //Who creates the dummy response packet?
                DPRINTF(RubySNI, "SNI: What went wrong?\n");
                assert(false);
                return false;
            }
    }
    }
    else if (responder_id !=-1){
        if ((t_flit->get_msg_ptr())->getType() != CoherenceResponseType_ACK){
            assert(false);
        }
    }

    //Don't think we should ever end up here
    return false;
}

