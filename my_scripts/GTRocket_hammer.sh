NUM_CPUS=64
NUM_CHIPLETS=8

./build/X86_MOESI_hammer/gem5.opt \
-d my_outdir \
--debug-flags=RubySNI \
configs/example/se.py \
--cpu-type TimingSimpleCPU \
--num-cpus=${NUM_CPUS} \
--l1d_size=4kB --l1i_size=4kB --l1d_assoc=4 \
--num-l2caches=${NUM_CPUS} \
--l2_size=8kB --l2_assoc=4 \
--num-dirs=4 \
--ruby --mem-size=1024MB \
--num-chiplets=${NUM_CHIPLETS} \
--network=garnet2.0 \
--vcs-per-vnet=4 \
--topology=CHIPS_GTRocket_Mesh_hammer \
-c "m5threads/tests/array_sum" \
-o "2"

# Print Stats
echo
echo "CHIPS Demo: Stats:"
grep "sim_ticks" my_outdir/stats.txt
grep "average_packet_latency" my_outdir/stats.txt
