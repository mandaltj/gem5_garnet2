#This script is just to test the topology and the connections
#between the cache controllers
#--debug-flags=RubyNetwork \
MAX_INSTR=$1
DEBUG=$2

echo "Number of Max Instructions: ${MAX_INSTR}"
echo "Debug: ${DEBUG}"

./build/X86_MOESI_hammer/gem5.opt \
-d my_outdir \
--debug-flags=RubyNetwork \
configs/example/se.py \
--cpu-type TimingSimpleCPU \
--num-cpus=64 \
--l1d_size=16kB --l1i_size=16kB --l1d_assoc=4 \
--l2_size=128kB --l2_assoc=4 \
--num-dirs=4 \
--ruby --mem-size=4096MB \
--num-chiplets=8 \
--network=garnet2.0 \
--topology=CHIPS_GTRocket_Mesh \
-c "m5threads/tests/test_atomic" \
-o "2"

# Print Stats
echo
echo "CHIPS Demo: Stats:"
grep "sim_ticks" my_outdir/stats.txt
grep "average_packet_latency" my_outdir/stats.txt
