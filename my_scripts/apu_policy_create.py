def write_apu_file(file_name_string, policy):
    file_obj = open(file_name_string, 'w')
    for policy_idx in range(len(policy)):
        for idx in range(4):
            if idx==3 and policy_idx!=(len(policy)-1):
                file_obj.write(str(policy[policy_idx][idx])+"\n")
            else:
                file_obj.write(str(policy[policy_idx][idx])+" ")
    file_obj.close()

def compress_policy(apu_policy, page_diff):
    for i in range(len(apu_policy)):
        for j in range(i+1, len(apu_policy)):
            if(apu_policy[i][0] == apu_policy[j][0]):
                if ((abs(apu_policy[j][1] - apu_policy[i][1])/4096)
                    == page_diff):
                    #print(str(i) + " " + str(apu_policy[i]))
                    #print(str(j) + " " + str(apu_policy[j]))
                    if apu_policy[j][1] >= apu_policy[i][1]:
                        if (apu_policy[i][3] == 1):
                            #Going to remove jth apu_policy
                            apu_policy[i][2] = apu_policy[j][2]
                            #print("0 Removing index : " + str(j) +
                            #" " + str(i) +" Policy: " + str(apu_policy[j]))
                            del apu_policy[j]
                            return apu_policy, True
                        elif (apu_policy[j][3] == 1):
                            #Going to remove ith apu_policy
                            apu_policy[j][1] = apu_policy[i][1]
                            #print("1 Removing index : " + str(i) +
                            #" " + str(j) +" Policy: " + str(apu_policy[i]))
                            del apu_policy[i]
                            return apu_policy, True
                        else:
                            #Going to remove jth apu_policy
                            apu_policy[i][2] = apu_policy[j][2]
                            #print("2 Removing index : " + str(j) +
                            #" " + str(i) +" Policy: " + str(apu_policy[j]))
                            del apu_policy[j]
                            return apu_policy, True
                    else:
                        if (apu_policy[i][3] == 1):
                            #Going to remove jth apu_policy
                            apu_policy[i][1] = apu_policy[j][1]
                            #print("3 Removing index : " + str(j) +
                            #" " + str(i) +" Policy: " + str(apu_policy[j]))
                            del apu_policy[j]
                            return apu_policy, True
                        elif (apu_policy[j][3] == 1):
                            #Going to remove ith apu_policy
                            apu_policy[j][2] = apu_policy[i][2]
                            #print("4 Removing index : " + str(i) +
                            #" " + str(i) +" Policy: " + str(apu_policy[i]))
                            del apu_policy[i]
                            return apu_policy, True
                        else:
                            #Going to remove ith apu_policy
                            apu_policy[j][2] = apu_policy[i][2]
                            #print("5 Removing index : " + str(i) +
                            #" " + str(j) +" Policy: " + str(apu_policy[i]))
                            del apu_policy[i]
                            return apu_policy, True

    return apu_policy, False

"""
    Read File to get the memory accesses
"""

file_name = open('L2_Requests.txt', 'r')

line_addr = []

while True:
    line = file_name.readline()
    if not line:
        break

    if (line[9:12] == 'Req'):
        hex_str = ""
        for c in line[31:]:
            if(c==","):
                break
            hex_str += c
        Addr_hex = hex_str
        Addr_int = int(hex_str, 16)
        #print(Addr_hex)
        #print(Addr_int)

    count = 0
    for c in line[32:]:
        count += 1
        if(c=="T"):
            Req_type = line[32+count:].split()[2]
            if(Req_type == "GETS" or Req_type == "GETX"):
                Req_bit = 0
            elif(Req_type == "PUTX"):
                Req_bit = 1
            else:
                print("Unknows Request Type")
                exit()
            break

    for item in line.split():
        if "L2Cache" in item:
            Chiplet_id = int(item[8])

    line_addr.append([Addr_hex, Addr_int, Req_type, Req_bit, Chiplet_id])

file_name.close()

"""
    Code to generate page address ranges based on the sample of the L2_Requests
"""

apu_policy = []
frame_list = []

for item in line_addr:
    frame_addr = (item[1]/4096)
    if frame_addr in frame_list:
        temp_index = frame_list.index(frame_addr)
        if ((line_addr[temp_index][3] == 0) and (item[3] == 1)):
            line_addr[temp_index][3] = 1
            continue

    frame_list.append(frame_addr);
    start_addr = (item[1]/4096) * 4096
    end_addr = (start_addr + 4096)-1
    apu_policy.append([item[4], start_addr, end_addr, item[3]])

#apu_policy.sort(key=lambda x: x[1])
#apu_policy.sort(key=lambda x: x[3])

##print(line_addr)
print("APU Policy Size: " + str(len(apu_policy)))

write_apu_file("apu_all.txt", apu_policy)


page_diff = 0
while(len(apu_policy) > 32):
    #print("page diff: " + str(page_diff))
    apu_policy, test = compress_policy(apu_policy, page_diff)
    ##print(len(apu_policy))
    if not test:
        page_diff += 1
    #if (page_diff==10):
    #    break

write_apu_file("apu_init.txt", apu_policy)
