#!/bin/bash
rm -f Memory_accesses.txt L1_Requests.txt L2_Requests.txt
sed -n '/network_links/ p' output_GTRocket_Mesh_hammer_RubyNetwork.txt > Memory_accesses.txt
sed -i '/RequestMsg\|ResponseMsg/!d' Memory_accesses.txt
sed -i 's/^.*\(\bMessage\b\)/\1/g' Memory_accesses.txt
sed '/Requestor = L2/!d' Memory_accesses.txt > L2_Requests.txt
sed '/Requestor = L1/!d' Memory_accesses.txt > L1_Requests.txt
